const luisHost = "https://westus.api.cognitive.microsoft.com/luis/api/v2.0";
const luisWebHost = "https://westus.api.cognitive.microsoft.com/luis/webapi/v2.0";
const luisHeader = {
  "Ocp-Apim-Subscription-Key": "c9ded765a2f846919caccc6e7d04681c"
}

export function getModels() {
    let options = {
        headers: luisHeader,
    };
    return fetch(luisHost + "/apps", options);
};


export function getIntents(model, intentID) {
    let options = {
        headers: luisHeader,
    };
    return fetch(luisHost + `/apps/${model.id}/versions/${model.activeVersion}/intents`, options);
}

export function getUtterances(model, intent){
  let options = {
    headers: luisHeader,
  };
  return fetch(luisWebHost + `/apps/${model.id}/versions/${model.activeVersion}/models/${intent.id}/reviewLabels`, options);
}


export function dispatchRefresh() {
  console.log('Refreshing bot dispatch: ')

  let url = 'https://jnjchatbot.azurewebsites.net/dispatch'

  async function api(){
  const response = await fetch(url, {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      credentials: 'omit', // include, *same-origin, omit
      headers: {
      'Content-Type': 'application/json'
      }
  });

  // console.log("Await Response: ");
  let res = await response.text()
  res = JSON.stringify(res, null, 4);
  console.log("Dispatch refresh await Response: ", res);
  }

  api()
  
}