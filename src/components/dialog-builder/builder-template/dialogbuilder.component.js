import React from "react";
import "./dialogbuilder.css";
import ReactDOM from "react-dom";
import {syncFetchAPICall, syncFetchAPICallDev, syncFetchDialogAPICall} from "../../services";
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap";
import ChatWidget from '../../chatWidget'

const ChoiceMarkup = props => {
  //pulls instance of the DialogBuilder Class to call the respective state setter functions
  const instance = props.instance;
  switch (props.step.construct) {
    case "CONFIRM_PROMPT":
      return (
        <div className="form-group mb-5">
          <label htmlFor={"step-prompt-" + props.index}>Prompt Question</label>
          <input
            type="text"
            name={"step-prompt-" + props.index}
            data-id={props.index}
            id={"step-prompt-" + props.index}
            className="form-control"
            value={props.step.promptOptions.prompt}
            onChange={event => instance.handlePromptChange(props.index, event)}
          />
        </div>
      );
      break;
    case "CHOICE_PROMPT":
      return (
        <div>
          <div className="form-group mb-5">
            <label htmlFor={"step-prompt-" + props.index}>Prompt Question</label>
            <input
              type="text"
              name={"step-prompt-" + props.index}
              data-id={props.index}
              id={"step-prompt-" + props.index}
              className="form-control"
              value={props.step.promptOptions.prompt}
              onChange={event => instance.handlePromptChange(props.index, event)}
            />
          </div>
          <div className="form-row mb-4 mt-4">
            <div className="form-group col-12">
              Add the group of clickable options to be shown for this prompt -<strong> {props.step.promptOptions.choices.length} Added</strong>
            </div>
            {props.step.promptOptions.choices.map((choice, index) => {
              var elementID = "step-" + props.index + "-choice-" + index;
              return (
                <div className="form-group col-md-4" key={index}>
                  <label
                    htmlFor={elementID}
                    style={{
                      fontSize: "12px"
                    }}>
                    {" "}
                    Option {`${index + 1}`}
                  </label>
                  <input
                    type="text"
                    name={elementID}
                    data-id={index}
                    value={choice}
                    id={elementID}
                    className="form-control form-control-sm"
                    placeholder="Identifier"
                    onChange={event => instance.handleChoiceOptionChange(props.index, index, event)}
                  />
                </div>
              );
            })}
            <div className="form-group col-md-4">
              <div className="button">
                <label
                  htmlFor="inputEmail4"
                  style={{
                    fontSize: "12px"
                  }}>
                  &nbsp;
                </label>
                <div className="btn btn-sm btn-block btn-outline-secondary pointer" onClick={e => instance.addOptionToChoicePrompt(props.index, e)}>
                  <small>
                    <span className="fa fa-fw fa-plus" /> Add New Choice
                  </small>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
      break;
    case "NAME_PROMPT":
      return (
        <div className="form-group mb-5">
          <label htmlFor={"step-prompt-" + props.index}>Prompt Question</label>
          <input
            type="text"
            name={"step-prompt-" + props.index}
            data-id={props.index}
            id={"step-prompt-" + props.index}
            className="form-control"
            value={props.step.promptOptions.prompt}
            onChange={event => instance.handlePromptChange(props.index, event)}
          />
        </div>
      );
      break;
    case "NUMBER_PROMPT":
      return (
        <div>
          <div className="form-group mb-5">
            <label htmlFor={"step-prompt-" + props.index}>Prompt Question</label>
            <input
              type="text"
              name={"step-prompt-" + props.index}
              data-id={props.index}
              id={"step-prompt-" + props.index}
              className="form-control"
              value={props.step.promptOptions.prompt}
              onChange={event => instance.handlePromptChange(props.index, event)}
            />
          </div>
          <div className="form-row">
            <div className="form-group col-sm-6">
              <label htmlFor={"step-prompt-" + props.index + "-minval"}>Minimum Value</label>
              <input
                type="text"
                name={"step-prompt-" + props.index + "-minval"}
                data-id={"step-prompt-" + props.index + "-minval"}
                id={"step-prompt-" + props.index + "-minval"}
                className="form-control"
                onChange={event => this.handlePromptOptValueChange(index, event, "minValue")}
                value={props.step.promptOptions.minValue}
              />
            </div>
            <div className="form-group col-sm-6">
              <label htmlFor={"step-prompt-" + props.index + "-maxval"}>Maximum Value</label>
              <input
                type="text"
                name={"step-prompt-" + props.index + "-maxval"}
                data-id={"step-prompt-" + props.index + "-maxval"}
                id={"step-prompt-" + props.index + "-maxval"}
                className="form-control"
                onChange={event => this.handlePromptOptValueChange(index, event, "maxValue")}
                value={props.step.promptOptions.maxValue}
              />
            </div>
          </div>
        </div>
      );
      break;
    default:
  }
};

class DialogBuilder extends React.Component {
  constructor(props, history) {
    console.log("constructor console.log", props.match.params.dialogID);
    super(props);
    // const
    this.state = {
      new: true,
      dialogName: "",
      dialogDescription: "",
      dialogTriggerIntent: "",
      dialogID: "",
      steps: [
        {
          typeValue: "nonAPI",
          name: "",
          construct: "NAME_PROMPT"
        }
      ],

      apiIntegrationSteps: [
        {
          identifier: "",
          value: ""
        }
      ],
      modal: false
    };

    this.URLData = [
      {
        url: "localhost:3000/api",
        name: "local"
      },
      {
        url: "https://dev2.jnjux.com/api/serverscript/erp_api_connect/CALLAPI",
        name: "devAPI"
      }
    ];

    this.testApiStep_backup = {
      url: "https://dev2.jnjux.com/api/serverscript/erp_api_connect/CALLAPI",
      payload: [
        {
          param: "SYSTEM",
          value: ""
        },
        {
          param: "ORDERID",
          value: ""
        },
        {
          param: "CONTROLID",
          value: ""
        }
      ]
    };

    this.testApiStep = {
      url: "",
      payload: []
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.toggle = this.toggle.bind(this);
    this.testAPI = this.testAPI.bind(this);

    // console.log('Location State: ', this.props.location.state);

    if (this.props.location.state) {
      this.fetchNavigatedRouteData(this.props.location.state);
    }
  }
  componentDidMount() {}

  fetchNavigatedRouteData = locationState => {
    console.log("--->inside fetch", location.steps);
    this.state.dialogName = locationState.name;
    this.state.dialogDescription = locationState.description;
    this.state.dialogTriggerIntent = locationState.intent;
    this.state.dialogID = "abc";
    this.state.steps = locationState.steps;
    var apiSteps = {};
    this.state.apiIntegrationSteps.pop();

    if (!locationState.new) {
      Object.keys(locationState.apiIntegrationSteps).forEach(key => {
        apiSteps = {};
        apiSteps["identifier"] = key;
        apiSteps["value"] = locationState.apiIntegrationSteps[key];
        // console.log('--->apiSteps', apiSteps)
        this.state.apiIntegrationSteps.push(apiSteps);
      });
    }

    this.state.new = locationState.new;
  };

  addStep = (typeValue, index, e) => {
    let mutated = this.state.steps.slice();
    if (typeValue == "nonAPI") {
      var newStep = {
        construct: "NAME_PROMPT",
        name: "",
        promptOptions: {prompt: ""},
        typeValue: "nonAPI"
      };
    } else {
      var newStep = {
        typeValue: typeValue,
        name: "summary",
        url: "",
        msg: ""
      };
    }
    mutated.splice(index, 0, newStep);
    this.setState({steps: mutated});
  };

  addClosingStep = e => {
    //console.log('inside closing step')
  };

  addOptionToChoicePrompt = (stepIndex, e) => {
    const mutated = this.state.steps.map((step, index) => {
      if (index == stepIndex) {
        step.promptOptions.choices.push("");
      }
      return step;
    });
    this.setState({steps: mutated});
  };

  addIdnValue = e => {
    //console.log(this.state, 'statee');

    this.setState(prevState => ({
      apiIntegrationSteps: [
        ...prevState.apiIntegrationSteps,
        {
          identifier: "",
          value: ""
        }
      ]
    }));
    //console.log(this.state, 'statee');
    e.preventDefault();
  };

  handlePromptTypeChange = (stepIndex, event) => {
    const mutated = this.state.steps.map((step, index) => {
      if (index == stepIndex) {
        step.construct = event.target.value;
        step.promptOptions = this.addRespectivePromptOpts(stepIndex, step.promptOptions, event.target.value);
      }
      return step;
    });
    this.setState({steps: mutated});
  };

  addRespectivePromptOpts = (stepIndex, promptOptions, construct) => {
    if (construct == "CHOICE_PROMPT") {
      //empty choices array needs to be added if the choice prompt is chosen
      return {...promptOptions, ...{choices: []}};
    } else if (construct == "NUMBER_PROMPT") {
      //min max values need to be added if number prompt is chosen
      return {...promptOptions, ...{minValue: 0, maxValue: 0}};
    } else {
      return promptOptions;
    }
  };

  handlePromptOptValueChange = (stepIndex, event, identifier) => {
    //identifier is the JSON key of value that needs to be changed
    const mutated = this.state.steps.map((step, index) => {
      if (index == stepIndex) {
        step.promptOptions[identifier] = event.target.value;
      }
      return step;
    });
    this.setState({steps: mutated});
  };

  handleChangeDialogName = e => {
    // console.log(e.target.value)
    this.setState({dialogName: event.target.value});
  };

  handleChangeStepName = (index, event) => {
    var stateCopy = Object.assign({}, this.state);
    stateCopy.steps[index].name = event.target.value;
    this.setState(stateCopy);
  };

  handlePromptChange = (index, event) => {
    //console.log(this.state.steps)
    var stateCopy = Object.assign({}, this.state);
    var json = stateCopy.steps[index];
    "promptOptions" in json
      ? (stateCopy.steps[index].promptOptions["prompt"] = event.target.value)
      : (stateCopy.steps[index]["promptOptions"] = {
          prompt: event.target.value
        });

    this.setState(stateCopy);
  };

  handleMinValueChange = (index, event) => {
    var stateCopy = Object.assign({}, this.state);
    var json = stateCopy.steps[index];
    "promptOptions" in json
      ? (stateCopy.steps[index].promptOptions["minValue"] = event.target.value)
      : (stateCopy.steps[index]["promptOptions"] = {
          minValue: event.target.value
        });

    this.setState(stateCopy);
  };

  handleMaxValueChange = (index, event) => {
    var stateCopy = Object.assign({}, this.state);
    var json = stateCopy.steps[index];
    "promptOptions" in json
      ? (stateCopy.steps[index].promptOptions["maxValue"] = event.target.value)
      : (stateCopy.steps[index]["promptOptions"] = {
          maxValue: event.target.value
        });
    this.setState(stateCopy);
  };

  handleChoiceOptionChange = (stepIndex, choiceIndex, event) => {
    const mutated = this.state.steps.map((step, index) => {
      if (index == stepIndex) {
        step.promptOptions.choices[choiceIndex] = event.target.value;
      }
      return step;
    });
    this.setState({steps: mutated});
  };

  handleURLChange = (index, event) => {
    var stateCopy = Object.assign({}, this.state);
    stateCopy.steps[index].url = event.target.value;

    this.setState(stateCopy);
    event.preventDefault();
  };

  handleMsgChange = (index, event) => {
    var stateCopy = Object.assign({}, this.state);
    stateCopy.steps[index].msg = event.target.value;

    this.setState(stateCopy);
    event.preventDefault();
  };

  captureTestApiValue = (e, indx) => {
    // console.log('In capture for Test Api:', e, ': ', indx);
    this.testApiStep.payload[indx] = e.target.value;
  };

  handleChangeIdentifier(index, idx, event) {
    var stateCopy = Object.assign({}, this.state);
    stateCopy.apiIntegrationSteps[index].identifier = event.target.value;
    this.setState(stateCopy);
    let tempJSON = {};
    this.state.apiIntegrationSteps.forEach(element => {
      if (element.identifier)
        //console.log('option', element)
        tempJSON[element.identifier] = element.value;
    });
    //console.log(tempJSON)
    var newState = [...this.state.steps];
    var json = newState[idx];
    "body" in json ? (newState[idx].body = tempJSON) : (newState[idx]["body"] = tempJSON);

    this.setState({steps: newState});
  }

  handleChangeIdentifierValue(index, idx, event) {
    var stateCopy = Object.assign({}, this.state);
    stateCopy.apiIntegrationSteps[index].value = event.target.value;
    this.setState(stateCopy);
    let tempJSON = {};
    this.state.apiIntegrationSteps.forEach(element => {
      if (element.identifier)
        //console.log('option', element)
        tempJSON[element.identifier] = element.value;
    });
    // console.log(tempJSON)
    var newState = [...this.state.steps];
    var json = newState[idx];
    "body" in json ? (newState[idx].body = tempJSON) : (newState[idx]["body"] = tempJSON);

    this.setState({steps: newState});
  }

  // added code
  async testAPI() {
    console.log("Triggering API call");
    var dataToPost = {};
    for (let indx in this.testApiStep.payload) {
      let testValue = this.testApiStep.payload[indx];
      console.log("test value : ", testValue);
      dataToPost[this.state.apiIntegrationSteps[indx]["identifier"]] = testValue;
    }

    /*    let dataToPost = {
      "SYSTEM": "ROTC-MX2",
      "CONTROLID": "ORDSTATUS", //ORDSTATUS,
      "ORDERID": "0100000007"
    } */

    this.testApiStep.url = this.state.steps[this.state.steps.length - 1]["url"];
    console.log("test url:", this.testApiStep.url);
    console.log("Data to post:", dataToPost);

    // api(this.testApiStep.url, dataToPost)
    try {
      document.getElementById("spin").removeAttribute("hidden");
      let res = await syncFetchAPICallDev(this.testApiStep.url, dataToPost);
      res = JSON.stringify(res, null, 4);
      console.log("Await Response: ", res);
      const element = <div className="border border-primary p2">{res}</div>;
      document.getElementById("spin").setAttribute("hidden", "true");
      ReactDOM.render(element, document.getElementById("apiResId"));
    } catch (error) {
      console.log(error);
    }
  }

  handleSubmit = async event => {
    // console.log('form was submitted: ', this.state.steps, this.state);
    var finalJSON = {
      type: "waterfall",
      construct: "NAME_PROMPT",
      intent: this.state.dialogTriggerIntent,
      description: this.state.dialogDescription,
      // "intent": "CreateDelivery",
      // "description": "dialog for getting order flow",
      name: this.state.dialogName,
      steps: this.state.steps
    };

    console.log("final json", finalJSON);

    let res = await syncFetchAPICall("https://jnjchatbot.azurewebsites.net/dialogs", finalJSON, this.state.new);
    // res = JSON.stringify(res, null, 4);
    console.log("On Submit response:", res);
  };

  toggle() {
    console.log("");
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  deleteStep = (stepIndex, event) => {
    const mutated = this.state.steps.filter((step, index) => {
      if (index != stepIndex) {
        return step;
      }
    });
    this.setState({steps: mutated});
  };

  addStepIn = (event, idx) => {
    console.log("Before add:", this.state.steps);
    let curSteps = this.state.steps;
    curSteps.splice(idx, 0, this.step);
    console.log("After add:", curSteps);

    this.setState({steps: curSteps});
    event.preventDefault();
  };

  populateStepSugs = () => {
    // https://codepen.io/callmetimm/pen/GJGorp
    var keys = {},
      html = "";

    // create array
    ["aaa", "bbb", "ccc"].forEach(function(key) {
      keys[key] = 1;
    }); // generate js object keys

    Object.keys(keys) // convert keys to array
      // if mixed cases, pass: function (a, b) { return a.toLowerCase().localeCompare(b.toLowerCase());}
      .forEach(function(word) {
        html += '<option value="' + word + '">';
      });

    let htmlEle = html;
    // ReactDOM.render(htmlEle, document.getElementById("stepSugs"));
    // DOM.render(document.getElementById("stepSugs").appendChild(html)
    // $("#stepSugs").append(html);
  };

  change = event => {
    console.log(this.state);
  };

  render() {
    const isAPIStepAdd = this.state.isAPIStepAdd;
    let {steps, apiIntegrationSteps, options} = this.state;
    return (
      <section className="home">
        <div className="container mt-5">
          <div>
            <div className="section-name-wrapper weight-700">
              Dialog Models
              <span className="weight-400">and Builder</span>
            </div>
            <div>Build a bot dialog model and integrate it with your APIs.</div>
            <div className="separator mt-4 w-25">
              <hr />
            </div>
          </div>
          <div>
            {this.props.location.state ? (
              <div className="row mt-4">
                <div className="col-8">
                  <div className="card dialog-model">
                    <div className="card-body">
                      <div className="dialog-model-name weight-700">{this.state.dialogName}</div>
                      <div className="card-body mt-3">
                        <div className="step">
                          {this.props.location.state.new ? (
                            <div className="row  step-content">
                              <div className="col-1 text-center" />
                              <div className="col-11">
                                <div className="form-row">
                                  <label htmlFor="dialogNameInput">Good Job! Now add a few steps to your bot dialog</label>
                                </div>
                              </div>
                            </div>
                          ) : (
                            <div />
                          )}
                        </div>
                        <div className="step">
                          {steps.map((val, idx) => {
                            // console.log(val)
                            let stepNameId = `step-${idx}`,
                              constructId = `construct-${idx}`,
                              urlId = `urlId-${idx}`;
                            return this.state.steps[idx].typeValue == "nonAPI" ? (
                              <div key={idx} className="row mt-5 step step-content">
                                <div className="col-1 text-center">
                                  <div className="uppercase text-muted step-counter-text">
                                    <h3>{`${idx + 1}`}</h3>
                                    Step
                                  </div>
                                </div>
                                <div className="col-11 ">
                                  <div className="form-row">
                                    <div className="form-group col-md-8">
                                      <label htmlFor={stepNameId}>Step Name</label>
                                      <input
                                        name={stepNameId}
                                        data-id={idx}
                                        id={stepNameId}
                                        value={this.state.steps[idx].name}
                                        type="text"
                                        className="form-control"
                                        placeholder="Give a name to your step"
                                        onChange={event => this.handleChangeStepName(idx, event)}
                                      />
                                    </div>
                                    <div className="form-group col-md-4">
                                      <label htmlFor={constructId}>Prompt Type</label>
                                      <select id={constructId} className="form-control" value={this.state.steps[idx].construct} onChange={e => this.handlePromptTypeChange(idx, e)}>
                                        <option value="default" disabled="disabled">
                                          Select Here
                                        </option>
                                        <option value="NAME_PROMPT">Text</option>
                                        <option value="CHOICE_PROMPT">Choice</option>
                                        <option value="NUMBER_PROMPT">Number</option>
                                        <option value="CONFIRM_PROMPT">Confirm</option>
                                      </select>
                                    </div>
                                  </div>
                                  {/* {this.state.steps[idx].element} */}
                                  <div className="choice-render">
                                    <ChoiceMarkup step={val} index={idx} instance={this} />
                                  </div>
                                  <div className="mt-3">
                                    <div className="row">
                                      <div className="col-6">
                                        <button className="btn btn-sm btn-outline-info" onClick={event => this.addStep("nonAPI", idx + 1, event)}>
                                          <span className="fa fa-fw fa-plus" />
                                          Add Step Below
                                        </button>
                                      </div>
                                      <div className="col-6 text-right">
                                        <button className="btn btn-sm btn-outline-danger" onClick={event => this.deleteStep(idx, event)}>
                                          <span className="fa fa-fw fa-trash" />
                                          Delete Step
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            ) : (
                              <div key={idx} className="row mt-5 step">
                                <div className="col-1 text-center">
                                  <div className="uppercase text-muted step-counter-text">
                                    <h3>{`${idx + 1}`}</h3>
                                    Step
                                  </div>
                                </div>
                                <div className="col-11 step-content">
                                  <div className="integration-section">
                                    <div style={{fontSize: "20px"}}>Integrate with your API endpoint</div> <br />
                                    <div className="integration-section-form mt-2">
                                      <div className="form-group">
                                        <label htmlFor="inputAddress2">Endpoint URL</label>
                                        <input
                                          type="text"
                                          name={urlId}
                                          data-id={idx}
                                          id={urlId}
                                          className="form-control"
                                          list="browsers"
                                          value={this.state.steps[idx]["url"]}
                                          placeholder="http://lorem.ipsum"
                                          onChange={event => this.handleURLChange(idx, event)}
                                        />
                                        <datalist id="browsers">
                                          {this.URLData.map((e, key) => {
                                            return (
                                              <option key={key} value={e.url}>
                                                {e.url}
                                              </option>
                                            );
                                          })}
                                        </datalist>
                                      </div>
                                      <div className="form-group">
                                        <label htmlFor="inputAddress3">Message</label>
                                        <input
                                          type="text"
                                          name={urlId}
                                          data-id={idx}
                                          id={urlId}
                                          className="form-control"
                                          value={this.state.steps[idx]["msg"]}
                                          placeholder="Wait for api to return your result"
                                          onChange={event => this.handleMsgChange(idx, event)}
                                        />
                                      </div>
                                      <div>API Request Information</div>
                                      <div className="form-row mt-2">
                                        <div className="form-group col-md-8">
                                          <label
                                            style={{
                                              fontSize: "12px"
                                            }}>
                                            API parameter
                                          </label>
                                        </div>

                                        <div className="form-group col-md-4">
                                          <label
                                            style={{
                                              fontSize: "12px"
                                            }}>
                                            Link to a step
                                          </label>
                                        </div>
                                      </div>

                                      {apiIntegrationSteps.map((val, index) => {
                                        // console.log('val: ', val);
                                        // console.log('index: ', index);

                                        let identifierId = `identifier-${index}`,
                                          valueId = `valueId-${index}`;
                                        this.populateStepSugs();

                                        return (
                                          <div key={index}>
                                            <div className="form-row">
                                              <div className="form-group col-md-8">
                                                <input
                                                  name={identifierId}
                                                  data-id={index}
                                                  id={identifierId}
                                                  value={val["identifier"]}
                                                  type="text"
                                                  className="form-control"
                                                  placeholder="parameter"
                                                  onChange={e => {
                                                    this.handleChangeIdentifier(index, idx, event);
                                                  }}
                                                />
                                              </div>
                                              <div className="form-group col-md-4">
                                                <input
                                                  name={valueId}
                                                  data-id={index}
                                                  id={valueId}
                                                  list="stepSugs"
                                                  value={val["value"]}
                                                  type="text"
                                                  className="form-control"
                                                  placeholder="step"
                                                  onChange={e => {
                                                    this.handleChangeIdentifierValue(index, idx, event);
                                                  }}
                                                />
                                                {/* <input type="text" list="cars" />
                                                <datalist id="cars">
                                                  <option>Volvo</option>
                                                  <option>Saab</option>
                                                  <option>Mercedes</option>
                                                  <option>Audi</option>
                                                </datalist> */}
                                              </div>
                                            </div>
                                          </div>
                                        );
                                      })}
                                      <div className="form-group mb-3">
                                        <button className="btn btn-block btn-outline-primary btn-sm" onClick={this.addIdnValue}>
                                          <span className="fa fa-fw fa-plus" /> Add new API field
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="mt-3">
                                    <div className="row">
                                      <div className="col-6">
                                        <button className="btn btn-sm btn-outline-info mr-2" onClick={this.toggle}>
                                          Try it out!
                                        </button>
                                      </div>

                                      <div className="col-6 text-right">
                                        <button type="submit" className="btn btn-sm btn-outline-danger" onClick={event => this.deleteStep(idx, event)}>
                                          <span className="fa fa-fw fa-trash" />
                                          Delete Step
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            );
                          })}

                          <div className="row mt-5">
                            <div className="col-4" />
                            <div className="col-4">
                              <div className="form-group">
                                <button className="btn btn-block btn-outline-primary btn-sm" onClick={this.handleSubmit}>
                                  Save
                                </button>
                              </div>
                            </div>
                            <div className="col-4" />
                          </div>
                          {/* </form> */}
                        </div>
                        {/* api integration step */}
                        {/* add new step */}
                        <div className="row mt-5">
                          <div className="col-1 text-center">
                            <div className="uppercase text-muted step-counter-text">
                              NEW
                              <br />
                              Step
                            </div>
                          </div>
                          <div className="col-11">
                            <div
                              style={{
                                fontSize: "20px"
                              }}>
                              Choose one of the options to add the next step.
                            </div>
                            <br />
                            <div className="row add-prompt">
                              <div className="col-4">
                                <div className="card purple text-center">
                                  <div className="card-body" onClick={e => this.addStep("nonAPI", this.state.steps.length, e)}>
                                    <h4>
                                      <span className="fa fa-fw fa-question-circle-o" />
                                    </h4>
                                    <span className="weight-700">Message Prompt</span>
                                  </div>
                                </div>
                              </div>
                              <div className="col-4">
                                <div className="card yellow text-center">
                                  <div className="card-body" onClick={e => this.addStep("API", this.state.steps.length, e)}>
                                    <h4>
                                      <span className="fa fa-fw fa-link" />
                                    </h4>
                                    <span className="weight-700">API Integration</span>
                                  </div>
                                </div>
                              </div>
                              <div className="col-4">
                                <div className="card blue text-center">
                                  <div className="card-body" onClick={e => this.addStep("nonAPI", this.state.steps.length, e)}>
                                    <h4>
                                      <span className="fa fa-fw fa-quote-left" />
                                    </h4>
                                    <span className="weight-700">Closing Message</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-4">
                  <div className="card dialog-model">
                    <div className="card-body">
                      <div
                        className="card-title"
                        style={{
                          fontSize: "25px"
                        }}>
                        <span className="weight-700">Settings </span>
                        &amp; Info
                        <div className="w-25">
                          <hr />
                        </div>
                      </div>
                      <div className="card-content">
                        <div className="card-info">
                          <strong>Dialog Name</strong>
                          <br />
                          <p>{this.state.dialogName}</p>
                        </div>
                        <div className="card-info">
                          <strong>Triggered Intent</strong>
                          <br />
                          <p>{this.state.dialogTriggerIntent}</p>
                        </div>
                        <div className="card-info">
                          <strong>Description</strong>
                          <br />
                          <p>{this.state.dialogDescription}</p>
                        </div>
                        <div className="card-info">
                          <strong>API Integration</strong>
                          <br />
                          <p className="text-success">True</p>
                        </div>
                        <div className="card-Info">
                          {/* <button className="btn btn-sm btn-outline-warning" onClick={(e) => this.change(e)}>
                          <span className="fa fa-fw fa-bolt"></span>
                          Dummy test button
                        </button> */}
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="card dialog-model mt-4">
                    <div className="card-body">
                      <div
                        className="card-title"
                        style={{
                          fontSize: "25px"
                        }}>
                        <span className="weight-700">Available </span>
                        Fields
                        <div className="w-25">
                          <hr />
                        </div>
                      </div>
                      <div className="card-content">
                        <div className="card-info">
                          <strong>Prompt Message</strong>
                          <br />
                          <p>Prompt message asks user a questions, expecting an answer as an input.</p>
                        </div>
                        <div className="card-info">
                          <strong>Prompt Type</strong>
                          <br />
                          Type can be out of these three
                          <ul className="list-unstyled pl-3">
                            <li>• Text Prompt</li>
                            <li>• Choice Prompt</li>
                            <li>• Number Prompt</li>
                          </ul>
                        </div>
                        <div className="card-info">
                          <strong>API Integration</strong>
                          <br />
                          <p>Allows dialog model to send collected prompt data to the API of choice.</p>
                        </div>
                        <div className="card-info">
                          <strong>Closing Message</strong>
                          <br />
                          <p>Is essentially a last message in this dialog.</p>
                        </div>
                        <div className="card-info">
                          <strong>Message Prompt</strong>
                          <br />
                          <p>A Message expecting an answer from the user.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <div className="not-found card-body p-5 text-center">
                <div className="display-4">
                  <span className="fa fa-fw fa-unlink" />
                  <br />
                  Sorry!
                  <br />
                  But the resource you were looking for was not found.
                </div>
              </div>
            )}
          </div>
        </div>

        {/* MODAL CODE */}

        <div>
          <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} size="">
            <ModalHeader toggle={this.toggle} hidden="hidden">
              Modal title
            </ModalHeader>
            <ModalBody>
              <div className="card-body">
                <div className="modal-title weight-700">Test your API here</div>
                <div className="separator mt-3 w-100">
                  <hr />

                  <div className="integration-section-form mt-2">
                    <div className="form-group">
                      <label htmlFor="inputAddress2">Endpoint URL</label>
                      <input type="text" className="form-control" id="inputAddress2" value={this.state.steps[this.state.steps.length - 1]["url"]} readOnly />
                    </div>

                    <div className="integration-section-form mt-2">
                      <div>Request Payload</div>
                      <div className="form-row">
                        <div className="form-group col-md-8 ">
                          <label
                            style={{
                              fontSize: "14px"
                            }}>
                            API parameters
                          </label>
                        </div>

                        <div className="form-group col-md-4">
                          <label
                            style={{
                              fontSize: "14px"
                            }}>
                            Test Values
                          </label>
                        </div>
                      </div>
                    </div>
                    {this.state.apiIntegrationSteps.map((val, index) => {
                      let identifierId = `identifier-${index}`,
                        valueId = `valueId-${index}`;
                      return (
                        <div key={index}>
                          <div className="form-row">
                            <div className="form-group col-md-8">
                              <input name={identifierId} data-id={index} id={identifierId} type="text" className="form-control" value={val["identifier"]} readOnly />
                            </div>
                            <div className="form-group col-md-4">
                              <input
                                name={valueId}
                                data-id={index}
                                id={valueId}
                                type="text"
                                className="form-control"
                                placeholder=""
                                onChange={e => {
                                  this.captureTestApiValue(e, index);
                                }}
                              />
                            </div>
                          </div>
                        </div>
                      );
                    })}

                    <div className="form-row mt-2">
                      <div className="form-group mb-3 col-md-6">
                        <button className="btn btn-outline-primary btn-sm" onClick={this.testAPI}>
                          <span className="fa fa-fw fa-send-o mr-2" />
                          Test Request
                        </button>
                        <span id="spin" className="fa fa-spinner fa-pulse fa-lg ml-2" hidden />
                      </div>
                      <div className="form-group col-md-6 text-right">
                        <Button className="btn btn-outline-danger" onClick={this.toggle}>
                          Cancel
                        </Button>
                      </div>
                    </div>
                  </div>

                  <div className="modal-title weight-400">
                    <div className="p-2">API Response</div>

                    <div className="separator mt-3 w-100">
                      <hr />
                    </div>
                  </div>

                  <div id="apiResId" />
                </div>
              </div>
            </ModalBody>

            {/* <ModalFooter>
           <Button color="primary" onClick={this.toggle}>Do Something</Button>{' '}
           <Button color="secondary" onClick={this.toggle}>Cancel</Button>
         </ModalFooter> */}
          </Modal>
        </div>

        <datalist id="stepSugs" />
        <ChatWidget />
      </section>
      
      
    );
  }

  componentWillUnmount() {}
}
export default DialogBuilder;
