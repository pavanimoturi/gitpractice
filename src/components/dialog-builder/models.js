const models = [
  {
    "type": "waterfall",
    "construct": "NAME_PROMPT",
    "intent": "Weather",
    "description": "dialog for getting order status",
    "name": "ERP1",
    "steps": [
      {
        "name": "systemname",
        "construct": "CHOICE_PROMPT",
        "promptOptions": {
          "prompt": "Selectt an ERP system",
          "choices": ["ROTC-MX2", "BTB", "MERCURY"]
        }
      }, {
        "name": "controlID",
        "construct": "CHOICE_PROMPT",
        "promptOptions": {
          "prompt": "Select a sytem control type",
          "choices": ["ORDSTATUS", "ORDRELEASE", "ORDFLOW"]
        }
      }, {
        "name": "orderID",
        "construct": "NAME_PROMPT",
        "promptOptions": {
          "prompt": "Enter order ID "
        }
      }, {
        "name": "confirm",
        "construct": "CONFIRM_PROMPT",
        "promptOptions": {
          "prompt": "Do you want to proceed?"
        }
      }, {
        "name": "summary",
        "url": "https://dev2.jnjux.com/api/serverscript/erp_api_connect/CALLAPI",
        "body": {
          "SYSTEM": "systemname",
          "CONTROLID": "controlID",
          "ORDERID": "orderID"
        },
        "msg": "Please wait till we fetch information from SAP server"
      }
    ]
  }, {
    "type": "waterfall",
    "construct": "NAME_PROMPT",
    "intent": "BookFlight",
    "description": "dialog for getting order flow, and some other info",
    "name": "ERP2 Model Dialog",
    "steps": [
      {
        "name": "orderID",
        "construct": "NAME_PROMPT",
        "promptOptions": {
          "prompt": "Please enter your order ID"
        }
      }, {
        "name": "controlID",
        "construct": "CHOICE_PROMPT",
        "promptOptions": {
          "prompt": "Select a control ID",
          "choices": ["ORDSTATUS", "ORDRELEASE", "ORDFLOW"]
        }
      }, {
        "name": "systemname",
        "construct": "CHOICE_PROMPT",
        "promptOptions": {
          "prompt": "Please select an ERP system",
          "choices": ["ROTC-MX2", "BTB", "HAUL2"]
        }
      }, {
        "name": "confirm",
        "construct": "CONFIRM_PROMPT",
        "promptOptions": {
          "prompt": "Do you want to proceed?"
        }
      }, {
        "name": "summary",
        "url": "https://dev2.jnjux.com/api/serverscript/erp_api_connect/CALLAPI",
        "body": {
          "SYSTEM": "systemname",
          "CONTROLID": "controlID",
          "ORDERID": "orderID"
        },
        "msg": "Hold on, I am fetching information from server"
      }
    ]
  }, {
    "type": "waterfall",
    "construct": "NAME_PROMPT",
    "intent": "Weather",
    "description": "dialog for getting order status",
    "name": "Credit Information",
    "steps": [
      {
        "name": "systemname",
        "construct": "CHOICE_PROMPT",
        "promptOptions": {
          "prompt": "Selectt an ERP system",
          "choices": ["ROTC-MX2", "BTB", "MERCURY"]
        }
      }, {
        "name": "controlID",
        "construct": "CHOICE_PROMPT",
        "promptOptions": {
          "prompt": "Select a sytem control type",
          "choices": ["ORDSTATUS", "ORDRELEASE", "ORDFLOW"]
        }
      }, {
        "name": "orderID",
        "construct": "NAME_PROMPT",
        "promptOptions": {
          "prompt": "Enter order ID "
        }
      }, {
        "name": "confirm",
        "construct": "CONFIRM_PROMPT",
        "promptOptions": {
          "prompt": "Do you want to proceed?"
        }
      }, {
        "name": "summary",
        "url": "https://dev2.jnjux.com/api/serverscript/erp_api_connect/CALLAPI",
        "body": {
          "SYSTEM": "systemname",
          "CONTROLID": "controlID",
          "ORDERID": "orderID"
        },
        "msg": "Please wait till we fetch information from SAP server"
      }
    ]
  }, {
    "type": "waterfall",
    "construct": "NAME_PROMPT",
    "intent": "BookFlight",
    "description": "dialog for getting order flow",
    "name": "Flight Booking",
    "steps": [
      {
        "name": "orderID",
        "construct": "NAME_PROMPT",
        "promptOptions": {
          "prompt": "Please enter your order ID"
        }
      }, {
        "name": "controlID",
        "construct": "CHOICE_PROMPT",
        "promptOptions": {
          "prompt": "Select a control ID",
          "choices": ["ORDSTATUS", "ORDRELEASE", "ORDFLOW"]
        }
      }, {
        "name": "systemname",
        "construct": "CHOICE_PROMPT",
        "promptOptions": {
          "prompt": "Please select an ERP system",
          "choices": ["ROTC-MX2", "BTB", "HAUL2"]
        }
      }, {
        "name": "confirm",
        "construct": "CONFIRM_PROMPT",
        "promptOptions": {
          "prompt": "Do you want to proceed?"
        }
      }, {
        "name": "summary",
        "url": "https://dev2.jnjux.com/api/serverscript/erp_api_connect/CALLAPI",
        "body": {
          "SYSTEM": "systemname",
          "CONTROLID": "controlID",
          "ORDERID": "orderID"
        },
        "msg": "Hold on, I am fetching information from server"
      }
    ]
  }, {
    "type": "waterfall",
    "construct": "NAME_PROMPT",
    "intent": "Weather",
    "description": "dialog for getting order status",
    "name": "Checking Account",
    "steps": [
      {
        "name": "systemname",
        "construct": "CHOICE_PROMPT",
        "promptOptions": {
          "prompt": "Selectt an ERP system",
          "choices": ["ROTC-MX2", "BTB", "MERCURY"]
        }
      }, {
        "name": "controlID",
        "construct": "CHOICE_PROMPT",
        "promptOptions": {
          "prompt": "Select a sytem control type",
          "choices": ["ORDSTATUS", "ORDRELEASE", "ORDFLOW"]
        }
      }, {
        "name": "orderID",
        "construct": "NAME_PROMPT",
        "promptOptions": {
          "prompt": "Enter order ID "
        }
      }, {
        "name": "confirm",
        "construct": "CONFIRM_PROMPT",
        "promptOptions": {
          "prompt": "Do you want to proceed?"
        }
      }, {
        "name": "summary",
        "url": "https://dev2.jnjux.com/api/serverscript/erp_api_connect/CALLAPI",
        "body": {
          "SYSTEM": "systemname",
          "CONTROLID": "controlID",
          "ORDERID": "orderID"
        },
        "msg": "Please wait till we fetch information from SAP server"
      }
    ]
  }, {
    "type": "waterfall",
    "construct": "NAME_PROMPT",
    "intent": "BookFlight",
    "description": "dialog for getting order flow Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora, exercitationem nisi voluptas quisquam possimus veniam in, ipsum sunt suscipit veritatis ipsa consectetur esse. Rerum, quam sunt, veniam nam voluptate aspernatur.",
    "name": "ERP2",
    "steps": [
      {
        "name": "orderID",
        "construct": "NAME_PROMPT",
        "promptOptions": {
          "prompt": "Please enter your order ID"
        }
      }, {
        "name": "controlID",
        "construct": "CHOICE_PROMPT",
        "promptOptions": {
          "prompt": "Select a control ID",
          "choices": ["ORDSTATUS", "ORDRELEASE", "ORDFLOW"]
        }
      }, {
        "name": "systemname",
        "construct": "CHOICE_PROMPT",
        "promptOptions": {
          "prompt": "Please select an ERP system",
          "choices": ["ROTC-MX2", "BTB", "HAUL2"]
        }
      }, {
        "name": "confirm",
        "construct": "CONFIRM_PROMPT",
        "promptOptions": {
          "prompt": "Do you want to proceed?"
        }
      }, {
        "name": "summary",
        "url": "https://dev2.jnjux.com/api/serverscript/erp_api_connect/CALLAPI",
        "body": {
          "SYSTEM": "systemname",
          "CONTROLID": "controlID",
          "ORDERID": "orderID"
        },
        "msg": "Hold on, I am fetching information from server"
      }
    ]
  }
];

export default models;

export function addToArray(object, instance) {
  instance.setState({
    // dialogModels : instance.state.dialogModels.concat(object)
  });
};
