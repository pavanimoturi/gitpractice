import React, {useState} from 'react'
import Moment from 'react-moment';
import {Link, withRouter} from 'react-router-dom';
import {ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText, Collapse, Table} from 'reactstrap';

import Utterances from './utterances.component'
import {getModels, getIntents, getUtterances} from '../../../services/luis.service'

import "./modelView.css";

function ListGroupCollapse(props) {
  const [collapse, setCollapse] = useState(false);
  const toggle = () => setCollapse(!collapse);

  console.log(props.examples)

  return (<ListGroupItem key={props.intent.id} className="pb-4">
    <div className="row pointer" onClick={toggle}>
      <div className="col-4">
        <span className="text-extra-muted uppercase extra-small weight-400">Intent</span>
        <br/>
        <span className="fs-20">{props.intent.name}</span>
      </div>
      <div className="col-4">
        <span className="text-extra-muted uppercase extra-small weight-400">Examples</span>
        <br/>
        <span className="fs-20">{props.examples.length}</span>
      </div>
      <div className="col-3">
        <span className="text-extra-muted uppercase extra-small weight-400">ID</span>
        <br/>
        <span className="small">{props.intent.id}</span>
      </div>
      <div className="col-1 pt-4 text-muted">
        {
          collapse
            ? (<span className="fa fa-fw fa-caret-down"></span>)
            : (<span className="fa fa-fw fa-caret-right"></span>)
        }

      </div>
    </div>
    <Collapse isOpen={collapse}>
      <hr className="mt-5 mb-4"/>
      <div className="mb-4">
        <span className="weight-600 fs-15">
          Available Utterance Examples
        </span>
      </div>
      <div className="examples-list-wrapper">
        {
          props.examples.length > 0
            ? (<Table>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Utterance</th>
                  <th>Score</th>
                  <th>Closest Intent</th>
                </tr>
              </thead>
              <tbody>
              {
                props.examples.map((example, index) => (
                  <tr>
                    <th scope="row">{index + 1}</th>
                    <td>{example.text}</td>
                    <td>{example.intentPredictions[0].score}</td>
                    <td>{example.intentPredictions[1].name} | {example.intentPredictions[1].score}</td>
                  </tr>
                ))
              }

              </tbody>
            </Table>)
            : (<div></div>)

        }
      </div>
    </Collapse>
  </ListGroupItem>)
}

class ModelView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      model: {},
      intents: [],
      examples: [],
      collapseToggle: false
    }

  }

  componentDidMount() {
    getIntents(this.props.location.state).then(response => response.json()).then(intents => {
      this.setState({intents: intents, model: this.props.location.state, loading: false})
    });
    // getUtterances(this.props.location.state).then(response => response.json()).then(data => {
    //   console.log(data)
    //   this.setState({examples: data, loading: false})
    // });
  }

  toggle = () => {
    this.setState({
      collapseToggle: !this.state.collapseToggle
    })
  }

  filterExamples = (intentName) => {
    const filtered = this.state.examples.filter((example) => example.intentLabel == intentName);
    return filtered;
  }

  render() {
    return (<section className="luis-models">
      <div className="container mt-5">
        <div>
          <div className="section-name-wrapper weight-700">
            LUIS Models
            <span className="weight-400">
              Management</span>
          </div>
          <div className="">
            Build a bot dialog model and integrate it with your APIs.
          </div>
          <div className="separator mt-4 w-25">
            <hr/>
          </div>
        </div>
      </div>

      {
        this.props.location.state
          ? (<div className="container mt-5">
            <ListGroup>
              {this.state.intents.map((intent, index) => (
                <Utterances model={this.props.location.state} key={intent.id} intent={intent} index={index} />))
              }

            </ListGroup>

          </div>)
          : (<div className="container text-center">
            <div className="not-fonund weight-400 fs-30">
              <span className="fa fa-fw fa-cog fa-spin"></span>
              <br/>
              Fetching your models
            </div>

          </div>)
      }

    </section>)
  }
}
export default ModelView
