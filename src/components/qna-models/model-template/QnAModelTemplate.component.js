import React from 'react'
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import {getScenarioQASets} from '../../../services/qna.service';

import './QnAModelTemplate.css';

const divAnswer = {
    height: '100px'
};

class QnAModelTemplate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            loading: true,
            sets: [],
            model: []
        };
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }

    componentDidMount() {
        getScenarioQASets(this.props.location.state.id).then(response => response.json()).then(data => {
            this.setState({model: this.props.location.state, sets: data.qnaDocuments, loading: false});
        })
    }

    render() {
        const modelName = this.state.model.name;

        return (<section className="luis-model-template">
            <div className="container mt-5">
                <div className="back-link small text-muted pointer hover-theme">
                    <span className="fa fa-fw fa-arrow-left"></span>
                    Back
                </div>
                <div>
                    <div className="section-name-wrapper weight-700">
                        {modelName}
                        <span className="weight-400">
              &nbsp;- Scenario QA Sets
            </span>
                    </div>
                    <div className="">
                        Build and Edit your question-answer scenarios here.
                    </div>
                    <div className="separator mt-4 w-25">
                        <hr/>
                    </div>
                    <div className="search-row row">
                        <div className="col-6 mt-3">
                            <div className="search-input w-50">
                                <input type="text" className="form-control new-response-input" placeholder="Search"
                                       name="newResponse"/>
                            </div>
                        </div>
                        <div className="col-6">
                            <div className="create-set-button float-right">
                                <button className="btn btn-outline-secondary button-primary">
                                    <span className="fa fa-fw fa-plus"></span>&nbsp; Create New Question-Answer Set
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {
                this.state.loading
                    ? (<div className="container">
                        <div className="text-center text-muted mt-5">
                            <div className="not-fonund weight-400 fs-30">
                                <span className="fa fa-fw fa-cog fa-spin"></span>
                                <br/>
                                Fetching your scenario QA sets
                            </div>
                        </div>
                    </div>)
                    : (<div className="container mt-4">
                        <div className="row">
                            {
                                this.state.sets.map((set, index) => (<div className="col-6 mb-4" key={set.id}>
                                    <div className="card set-card">
                                        <div className="card-body">
                                            <div className="uppercase text-muted weight-700 set-title h4 mb-3">
                                                Set {index + 1}
                                                <span className="float-right edit-icon">
                                                <span className="fa fa-fw fa-pencil"></span>
                                                Edit
                                            </span>
                                            </div>
                                            <div className="group group-answer" style={divAnswer}>
                                                <div className="weight-700 text-muted group-label">Answer :</div>
                                                <p className="group-content">
                                                    {set.answer}
                                                </p>
                                                <textarea className="form-control" id="exampleFormControlTextarea1" rows={2}
                                                          defaultValue={""} hidden="hidden"/>
                                            </div>
                                            <div className="group group-answer">
                                                <div className="weight-700 text-muted group-label">Questions
                                                    ({set.questions.length}) :
                                                </div>
                                                <div className="group-content questions-list">
                                                    {
                                                        set.questions.filter((set, index) => (index < 3)).map((question, index) => (
                                                            <div className="question-item mt-2" key={question}>
                                                                <span className="fa fa-fw fa-quote-left text-muted mr-2"></span>
                                                                <span>{question}</span>
                                                            </div>))
                                                    }

                                                    <div className="small text-muted mt-3">
                                                        <div className="cursor-pointer" onClick={this.toggle}>+ 3 More</div>
                                                        <Modal isOpen={this.state.modal} toggle={this.toggle}>
                                                            <ModalHeader toggle={this.toggle}>Modal title</ModalHeader>
                                                            <ModalBody>
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                            </ModalBody>
                                                            <ModalFooter>
                                                                <Button color="primary" onClick={this.toggle}>Do Something</Button>{' '}
                                                                <Button color="secondary" onClick={this.toggle}>Cancel</Button>
                                                            </ModalFooter>
                                                        </Modal>
                                                    </div>
                                                </div>
                                                <div className="group-content mt-2">
                                                    <div className="input-group input-group">
                                                        <input type="text" className="form-control new-response-input"
                                                               placeholder="Your question here" name="newResponse"/>
                                                        <div className="input-group-append">
                                                            <button className="btn btn-outline-info pointer" type="button;">
                                                                <span className="fa fa-fw fa-plus"/>
                                                                Add
                                                            </button>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>))
                            }
                        </div>
                    </div>)
            }
        </section>)
    }
}

export default QnAModelTemplate
