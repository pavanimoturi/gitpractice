import React from "react";
import Moment from "react-moment";
import {Link} from "react-router-dom";

//Styles
import "./qnaModels.css";
import {getModels} from "../../services/qna.service";
import {dispatchRefresh} from '../../services/luis.service'

/*const models = [
    {
        "_id": "5d8910f7384edf7971ecd3eb",
        "name": "Hours and Availability",
        "createdOn": "Thursday, April 3, 2014 7:55 PM",
        "updatedOn": "Saturday, January 9, 2016 4:12 PM"
    }, {
        "_id": "5d8910f78a4904f2ea60c629",
        "name": "System Status Questions",
        "createdOn": "Friday, July 6, 2018 8:14 AM",
        "updatedOn": "Saturday, December 30, 2017 10:13 PM"
    }, {
        "_id": "5d8910f773709b887113ae34",
        "name": "Miscellaneous",
        "createdOn": "Wednesday, January 1, 2014 9:56 PM",
        "updatedOn": "Sunday, December 16, 2018 1:04 AM"
    }, {
        "_id": "5d8910f7c7a9ea2b5848f571",
        "name": "Other Questions",
        "createdOn": "Sunday, July 13, 2014 11:56 AM",
        "updatedOn": "Wednesday, April 11, 2018 10:58 PM"
    }, {
        "_id": "5d8910f7e570cc826c8045df",
        "name": "Technical Questions",
        "createdOn": "Thursday, July 5, 2018 4:20 AM",
        "updatedOn": "Friday, June 19, 2015 2:09 PM"
    }
];*/

class QnAModels extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      models: []
    };
  }

  componentDidMount() {
    getModels()
      .then(response => response.json())
      .then(data => {
        this.setState({models: data.knowledgebases, loading: false});
      });
  }

  render() {
    return (
      <section className="luis-models">
        <div className="container mt-5">
          <div>
            <div className="section-name-wrapper weight-700">
              Question and Answer
              <span className="weight-400">&nbsp;Scenarios</span>
            </div>
            <div className="">Design and Edit your simple question and answer scenarios</div>
            <div className="separator mt-4 w-25">
              <hr />
            </div>
          </div>
        </div>
        {this.state.loading ? (
          <div className="container">
            <div className="text-center text-muted mt-5">
              <div className="not-fonund weight-400 fs-30">
                <span className="fa fa-fw fa-cog fa-spin" />
                <br />
                Fetching your models
              </div>
            </div>
          </div>
        ) : (
          <div className="container">
            <div className="create-new-button-row mt-4">
              <button className="btn btn-outline-secondary button-primary">
                <span className="fa fa-fw fa-plus" />
                Create New Q&A Scenario
              </button>
              <button className="btn btn-outline-primary button-secondary ml-2" onClick={dispatchRefresh}>
                <span className="fa fa-fw fa-refresh" />
                Refresh Bot Dispatch
              </button>
            </div>
            <div className="mt-4">
              <div className="list-headers">
                <div className="row models-row mb-2">
                  {this.state.models.map((model, index) => (
                    <div className="col-3 mb-4" key={model._id}>
                      <div className="card model-card">
                        <div className="card-body">
                          <Link
                            to={{
                              pathname: "/qna-models/" + model.id,
                              state: model
                            }}>
                            <div className="card-body-inner pointer">
                              <div className="extra-small uppercase text-extra-muted scenario-number weight-400">Scenario {index + 1}</div>
                              <div className="model-name qna-model-name weight-700 pointer smoothen-transition">
                                <span>{model.name}</span>
                              </div>
                              <div className="w-50">
                                <hr />
                              </div>
                              <div className="model-info">
                                <strong className="text-extra-muted small weight-600">QA Sets</strong>
                                <br />3
                              </div>
                              <div className="model-info">
                                <strong className="text-extra-muted small weight-600">Created On</strong>
                                <div>
                                  <Moment className="" format="MMM DD, YYYY, hh:mm A">
                                    {model.createdOn}
                                  </Moment>
                                </div>
                              </div>
                              <div className="model-info mt-2">
                                <strong className="text-extra-muted small weight-600">Last Updated</strong>
                                <div>
                                  <Moment className="" format="MMM DD, YYYY, hh:mm A">
                                    {model.updatedOn}
                                  </Moment>
                                </div>
                              </div>
                            </div>
                          </Link>

                          <div className="model-actions mt-4">
                            <button className="btn btn-block btn-sm btn-outline-danger">Delete</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        )}
      </section>
    );
  }
}

export default QnAModels;
