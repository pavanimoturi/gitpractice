import React from 'react'
import ReactDOM from 'react-dom'
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'

class Topbar extends React.Component {
  render() {
    return(
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container">
          <Link className="navbar-brand" to="/"><strong>Bot</strong>Builder</Link>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample09" aria-controls="navbarsExample09" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarsExample09">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item active">
                <Link className="nav-link" to="/dialog-builder">Dialog Builder</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/luis-models">Manage Intents</Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/qna-models">Manage FAQs</Link>
              </li>
              <li className="nav-item dropdown" hidden>
                <a className="nav-link dropdown-toggle" href="http://example.com" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                <div className="dropdown-menu" aria-labelledby="dropdown09">
                  <a className="dropdown-item" href="#">Action</a>
                  <a className="dropdown-item" href="#">Another action</a>
                  <a className="dropdown-item" href="#">Something else here</a>
                </div>
              </li>
            </ul>
            <form className="form-inline my-2 my-md-0 navbar-nav">
              <Link className="nav-link" to="/">Logout {process.env.MODE}<span className="fa fa-fw fa-sign-out"></span></Link>
            </form>
          </div>
        </div>

      </nav>
    )
  }
}
export default Topbar
